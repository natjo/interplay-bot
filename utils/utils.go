package utils

import "gitlab.com/natjo/interplay-bot/pieces"

func GetOtherPlayer(player int8) int8 {
	// Returns the number of the other player
	return (player + 1) % 2
}

func GetMinIndexRow(index int16, sizeX int8) int16 {
	return index - index%int16(sizeX)
}

func GetMaxIndexRow(index int16, sizeX int8) int16 {
	return index - index%int16(sizeX) + int16(sizeX) - 1
}

func Prepend(x []pieces.Piece, y pieces.Piece) []pieces.Piece {
	// Inserts into the beginning of the array, not the end
	x = append(x, 0)
	copy(x[1:], x)
	x[0] = y
	return x
}

func ReverseSlice(s []pieces.Piece) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}
