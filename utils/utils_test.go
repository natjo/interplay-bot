package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetMinIndexRow(t *testing.T) {
	// Given
	parameters := [][]int16{{0, 0}, {3, 0}, {8, 7}, {6, 0}}
	sizeX := int8(7)
	for _, params := range parameters {
		// When
		out := GetMinIndexRow(params[0], sizeX)
		// Then
		assert.Equal(t, params[1], out)
	}
}

func TestGetMaxIndexRow(t *testing.T) {
	// Given
	parameters := [][]int16{{0, 6}, {3, 6}, {8, 13}, {13, 13}}
	sizeX := int8(7)
	for _, params := range parameters {
		// When
		out := GetMaxIndexRow(params[0], sizeX)
		// Then
		assert.Equal(t, params[1], out)
	}
}
