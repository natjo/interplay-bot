# Interplay Bot

![interplay example](./media/interplay.webp)

## How to play
See `Makefile` on how to run and build this. By default the game is played against the computer. Set the environment variable `PLAY_VS_HUMAN` to any value to play against another human player.

Additionally, you can set the environment variable `INIT_GAME_RANDOM` to begin the game with some random pieces already played for each player.

## Structure
```
.
├── bot        # Code specifically for the bot, must follow the interface defined in `logic/game.go`
├── components # Reusable structs for separate components such as tiles on the field
├── logging
├── logic      # Code managing the main game flow, also connects between logic and ui
├── mcts       # Implementation of Monte-Carlo tree search
├── media      # Images used
├── pieces     # Resuable functionality to handle the different types of pieces, used within most other packages
├── README.md
├── test       # Collection of general tests to verify the bot reacts correctly
├── ui         # UI code, utilizing the fyne library
└── utils
```

## How to make svg images available within the go code
Simply run `gen_bundled_icons.sh` to transform the svg images within `/media` to usable go code. Only needs to be run on changes to the svg files.

## How to profile this code
- Import `net/http/pprof` in the `main.go` entrypoint
```golang
import (
	_ "net/http/pprof"
)
```
- Run a webserver to make the profiling available. Run this code at the start of `main()`
```golang
	go func() {
		logrus.Println(http.ListenAndServe("localhost:6060", nil))
	}()
```
- Generate a runtime profile by executing `curl http://127.0.0.1:6060/debug/pprof/profile > /tmp/cpu.prof` and while that runs generate some typical load
- See the visualization of the load with `go tool pprof -http :9402 /tmp/cpu.prof`
