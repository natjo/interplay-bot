package main

import (
	"os"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/interplay-bot/bot"
	"gitlab.com/natjo/interplay-bot/logging"
	"gitlab.com/natjo/interplay-bot/logic"
	"gitlab.com/natjo/interplay-bot/ui"
)

func main() {
	logging.InitLogging()
	size := 7

	app := app.New()
	window := app.NewWindow("Interplay")

	uiController := ui.NewController(size, size)
	var game *logic.Game
	if isP1Bot() {
		// Currently it's configured that the starting player is always 0 (human)
		b := bot.NewBot(int8(size), int8(size), 0)
		game = logic.NewGame(uiController, int8(size), int8(size), b)
	} else {
		logrus.Info("Playing against another human")
		game = logic.NewGameNoBot(uiController, int8(size), int8(size))
	}
	if isInitGameRandom() {
		game.InitRandom()
	}
	uiController.SetCallbackOnClick(game.OnTileSelection)

	window.SetContent(uiController.GetContainer())
	window.Resize(fyne.NewSize(400, 500))
	window.ShowAndRun()
}

func isP1Bot() bool {
	_, ok := os.LookupEnv("PLAY_VS_HUMAN")
	return !ok
}

func isInitGameRandom() bool {
	_, ok := os.LookupEnv("INIT_GAME_RANDOM")
	return ok
}
