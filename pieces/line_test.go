package pieces

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLineInit(t *testing.T) {
	// Given
	l := NewLine(2)
	// Then
	expected := make([]Piece, 4)
	expectedStart := 2
	expectedLength := 0
	assert.Equal(t, expected, l.pieces)
	assert.Equal(t, expectedStart, l.start)
	assert.Equal(t, expectedLength, l.length)
}

func TestAppend(t *testing.T) {
	// Given
	l := NewLine(3)

	// When
	l.Append(Stick)
	l.Append(Cylinder)
	l.Append(Stick)

	// Then
	expected := make([]Piece, 6)
	expected[3] = High
	expected[4] = Low
	expected[5] = High
	expectedStart := 3
	expectedLength := 3
	assert.Equal(t, expected, l.pieces)
	assert.Equal(t, expectedStart, l.start)
	assert.Equal(t, expectedLength, l.length)
}

func TestPrepend(t *testing.T) {
	// Given
	l := NewLine(2)

	// When
	l.Prepend(Low)
	l.Prepend(Stick)

	// Then
	expected := make([]Piece, 4)
	expected[2] = Low
	expected[1] = High
	expectedStart := 1
	expectedLength := 2
	assert.Equal(t, expected, l.pieces)
	assert.Equal(t, expectedStart, l.start)
	assert.Equal(t, expectedLength, l.length)
}
