package pieces

import "github.com/sirupsen/logrus"

// This manages a possible winning line
type Line struct {
	pieces        []Piece
	start, length int
}

func NewLine(size int) *Line {
	l := &Line{pieces: make([]Piece, size*2), start: size, length: 0}
	return l
}

func (l *Line) Prepend(p Piece) {
	if l.length > 0 {
		// If the first element is prepended, just add it directly to the current start
		l.start--
	}
	l.pieces[l.start] = ToHighLow(p)
	l.length++
}

func (l *Line) Append(p Piece) {
	l.pieces[l.start+l.length] = ToHighLow(p)
	l.length++
}

func (l *Line) ContainsWin() bool {
	for i := 0; i <= l.length-5; i++ {
		start := l.start + i
		end := l.start + 5 + i
		if isLineSectionWin(l.pieces[start:end]) {
			return true
		}
	}
	return false
}

func isLineSectionWin(p []Piece) bool {
	// Surrounded by Sticks
	if p[0] != High || p[4] != High {
		return false
	}
	// Symmetrical
	if p[1] != p[3] {
		return false
	}

	// At least one cylinder or tube in the middle part
	return p[1] == Low || p[2] == Low
}

func (l *Line) Print() {
	stringRepr := []string{}
	end := l.start + l.length
	for _, p := range l.pieces[l.start:end] {
		stringRepr = append(stringRepr, PieceName(p))
	}
	logrus.Info("Line: ", stringRepr)
}
