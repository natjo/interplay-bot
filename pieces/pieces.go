package pieces

type Piece int8

// Tube is the piece that can share a spot with a stick
// Cylinder is the piece that can't share a spot with anything on its tile
const (
	None Piece = iota
	Tube
	Cylinder
	Stick
	Low
	High
)

const InitPiecesStick = 10
const InitPiecesTube = 4
const InitPiecesCylinder = 4

func ToHighLow(p Piece) Piece {
	// Low and high generalize Stick vs Cylinder AND Tube
	if p == Stick || p == High {
		return High
	}
	if p == None {
		return None
	}
	return Low
}

func PieceName(p Piece) string {
	// Just a mapper to each name for debugging logs
	if p == Stick {
		return "Stick"
	} else if p == Tube {
		return "Tube"
	} else if p == Cylinder {
		return "Cylinder"
	} else if p == High {
		return "High"
	} else if p == Low {
		return "Low"
	}
	return "None"
}

func GetInitPieces() map[Piece]int8 {
	// Returns a map that represents the number of pieces a player starts with
	return map[Piece]int8{
		Stick:    InitPiecesStick,
		Tube:     InitPiecesTube,
		Cylinder: InitPiecesCylinder,
	}
}
