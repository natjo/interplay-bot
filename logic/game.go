package logic

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/interplay-bot/components"
	"gitlab.com/natjo/interplay-bot/pieces"
	"gitlab.com/natjo/interplay-bot/utils"
)

const humanPlayer = 0
const botPlayer = 1

// Holds the main state.
// On `piecesPlayerMustPlay: If a player picks up a piece he has to immediately place it again.
// This fields allows tracking this throught action turns
// On `currentPlayer`: We always set this to 0 initially, implying player 0 always starts
type Game struct {
	ui                  UI
	field               *components.Field
	hands               []*components.Hand
	piecePlayerMustPlay []pieces.Piece
	currentPlayer       int8
	isPlayer1Bot        bool
	bot                 Bot
}

type UI interface {
	SetPiece(int, []pieces.Piece)
	UpdatePieceSelection(int, map[pieces.Piece]int)
	MarkWinningTiles([]int)
}

type Bot interface {
	MakeDecision() (int16, pieces.Piece)
	UpdateFromAction(int16, pieces.Piece, int8)
}

func NewGame(ui UI, sizeX, sizeY int8, bot Bot) *Game {
	return &Game{
		ui:                  ui,
		field:               components.NewField(sizeX, sizeY),
		hands:               []*components.Hand{components.NewHand(), components.NewHand()},
		piecePlayerMustPlay: []pieces.Piece{pieces.None, pieces.None},
		currentPlayer:       0,
		isPlayer1Bot:        true,
		bot:                 bot,
	}
}

func NewGameNoBot(ui UI, sizeX, sizeY int8) *Game {
	// Two human players, as no bot is provided
	return &Game{
		ui:                  ui,
		field:               components.NewField(sizeX, sizeY),
		hands:               []*components.Hand{components.NewHand(), components.NewHand()},
		piecePlayerMustPlay: []pieces.Piece{pieces.None, pieces.None},
		currentPlayer:       0,
		isPlayer1Bot:        false,
		bot:                 nil,
	}
}

func (game *Game) InitRandom() {
	// To make the game more interesting, we fill the game with some random moves from each player
	r := rand.New(rand.NewSource(time.Now().Unix()))

	// Sticks
	for i := 0; i < 8; i++ {
		moveAllowed := false
		for !moveAllowed {
			index := int16(r.Int()) % game.field.GetLastIndex()
			moveAllowed = game.setPieceOnField(index, pieces.Stick)
		}
		game.nextPlayer()
	}

	// Cylinders
	for i := 0; i < 4; i++ {
		moveAllowed := false
		for !moveAllowed {
			index := int16(r.Int()) % game.field.GetLastIndex()
			moveAllowed = game.setPieceOnField(index, pieces.Cylinder)
		}
		game.nextPlayer()
	}

	// Tubes
	for i := 0; i < 4; i++ {
		moveAllowed := false
		for !moveAllowed {
			index := int16(r.Int()) % game.field.GetLastIndex()
			moveAllowed = game.setPieceOnField(index, pieces.Tube)
		}
		game.nextPlayer()
	}
}

func (game *Game) OnTileSelection(index int, selectedPieces []pieces.Piece) {
	// This is always triggered when the user makes a selection
	logrus.Debugf("Selected index %v by user %v", index, game.currentPlayer)
	game.runTurn(int16(index), selectedPieces[game.currentPlayer])
}

func (game *Game) runTurn(index int16, piece pieces.Piece) {
	// This is the main game loop
	logrus.Debugf("--Run turn for player %v--", game.currentPlayer)
	piece = game.ensureCorrectPiece(piece)
	if game.currentHand().IsPieceAvailable(piece) {
		moveAllowed := game.setPieceOnField(index, piece)
		if moveAllowed {
			logrus.Debugf("Place piece %v at %v", pieces.PieceName(piece), index)
			if game.checkIfPlayerWon(index, game.currentPlayer) {
				return
			}
			game.nextPlayer()
		}
	} else {
		// No piece is available. That could mean the user tries to pick up a piece
		logrus.Debugf("Player has no pieces of %v left", pieces.PieceName(piece))
		if game.currentHand().IsAllowedToRemove(piece) {
			wasRemoved := game.removePieceFromField(index, piece)
			if wasRemoved {
				logrus.Debugf("Remove piece %v at %v", pieces.PieceName(piece), index)
			}
		}
	}
	if game.isPlayer1Bot && game.currentPlayer == botPlayer {
		index, piece := game.bot.MakeDecision()
		game.runTurn(index, piece)
	}
}

func (game *Game) ensureCorrectPiece(piece pieces.Piece) pieces.Piece {
	if game.piecePlayerMustPlay[game.currentPlayer] != pieces.None {
		// This means in the last action the player picked up a piece to move,
		// the player HAS to directly use this piece
		piece = game.piecePlayerMustPlay[game.currentPlayer]
		game.piecePlayerMustPlay[game.currentPlayer] = pieces.None
	}
	return piece
}

func (game *Game) setPieceOnField(index int16, piece pieces.Piece) bool {
	placedPieces, moveAllowed := game.field.SetPiece(index, piece, game.currentPlayer)
	if !moveAllowed {
		logrus.Debug(fmt.Sprintf("Player can't place piece %v on this field", pieces.PieceName(piece)))
		return false
	}
	game.bot.UpdateFromAction(index, piece, game.currentPlayer)
	game.ui.SetPiece(int(index), placedPieces)
	game.reducePiecesLeft(piece, game.currentPlayer)
	return moveAllowed
}

func (game *Game) removePieceFromField(index int16, piece pieces.Piece) bool {
	placedPieces, wasRemoved := game.field.RemovePiece(index, piece, game.currentPlayer)
	if !wasRemoved {
		return false
	}
	game.piecePlayerMustPlay[game.currentPlayer] = piece
	game.bot.UpdateFromAction(index, piece, game.currentPlayer)
	game.ui.SetPiece(int(index), placedPieces)
	game.increasePiecesLeft(piece, game.currentPlayer)
	return wasRemoved
}

func (game *Game) reducePiecesLeft(piece pieces.Piece, player int8) {
	game.hands[player].ReducePiecesLeft(piece)
	game.updatePiecesLeftUI()
}

func (game *Game) increasePiecesLeft(piece pieces.Piece, player int8) {
	game.hands[player].IncreasePiecesLeft(piece)
	game.updatePiecesLeftUI()
}

func (game *Game) updatePiecesLeftUI() {
	// We have to transform from int8 to int
	piecesLeft := map[pieces.Piece]int{}
	for k, v := range game.currentHand().GetPiecesLeft() {
		piecesLeft[k] = int(v)
	}
	// Always show the pieces for human player
	game.ui.UpdatePieceSelection(int(game.currentPlayer), piecesLeft)
}

func (game *Game) nextPlayer() {
	game.currentPlayer = utils.GetOtherPlayer(game.currentPlayer)
}

func (game *Game) currentHand() *components.Hand {
	return game.hands[game.currentPlayer]
}

func (game *Game) checkIfPlayerWon(lastPlacedIndex int16, player int8) bool {
	line := game.field.CheckIfPlayerWon(lastPlacedIndex, player)
	if line != nil {
		logrus.Infof("Line of player %v contains a win", player)
		line.Print()
		// Optimally we should print the actual winning line, but to keep the line struct simple we and not
		// maintain this information, we just mark the winning tile
		game.ui.MarkWinningTiles([]int{int(lastPlacedIndex)})
		return true
	}
	return false
}
