.PHONY: run_debug run_info run run_vs_human test test_full test_no_cache

generate-mocks:
	mockery --all --inpackage

run_debug:
	LOG_LEVEL=DEBUG go run .

run_info:
	LOG_LEVEL=INFO go run .

run:
	go run .

run_vs_human:
	LOG_LEVEL=INFO PLAY_VS_HUMAN=1 go run .

test_full: generate-mocks
	go test ./...

test:
	go test ./...

test_no_cache:
	go clean -testcache
	go test ./...
