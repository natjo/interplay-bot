package components

import (
	"gitlab.com/natjo/interplay-bot/pieces"
	"gitlab.com/natjo/interplay-bot/utils"
)

// The field is a dictionary with the index as key. This results in a sparse matrix
type Field struct {
	tiles     map[int16]*Tile
	sizeX     int8
	sizeY     int8
	lastIndex int16
}

func NewField(sizeX, sizeY int8) *Field {
	return &Field{
		tiles:     map[int16]*Tile{},
		sizeX:     sizeX,
		sizeY:     sizeY,
		lastIndex: int16(sizeX)*int16(sizeY) - 1,
	}
}

func (field *Field) Clone() *Field {
	clonedField := &Field{
		tiles:     map[int16]*Tile{},
		sizeX:     field.sizeX,
		sizeY:     field.sizeY,
		lastIndex: field.lastIndex,
	}
	// Clone tiles
	for k, v := range field.tiles {
		clonedField.tiles[k] = v.Clone()
	}
	return clonedField
}

func (field *Field) GetTile(index int16) *Tile {
	// Ensures the selected tile does actually exist
	tile, exists := field.tiles[index]
	if !exists {
		tile = NewTile()
		field.tiles[index] = tile
	}
	return tile
}

func (field *Field) SetPieceAllowed(index int16, piece pieces.Piece, player int8) bool {
	tile, exists := field.tiles[index]
	if !exists {
		return true
	}
	return tile.SetPieceAllowed(piece, player)
}

func (field *Field) SetPiece(index int16, piece pieces.Piece, player int8) ([]pieces.Piece, bool) {
	// Returns if the action was successful and the current pieces on the indexed tile
	tile := field.GetTile(index)
	// This action will do nothing if the piece is not allowed on this tile
	moveAllowed := tile.SetPiece(piece, player)
	return tile.PlacedPieces, moveAllowed
}

func (field *Field) RemovePieceAllowed(index int16, piece pieces.Piece, player int8) bool {
	// Returns if the action would be successful
	if !field.existsTile(index) {
		return false
	}
	tile := field.GetTile(index)
	return tile.PlacedPieces[player] == piece
}

func (field *Field) RemovePiece(index int16, piece pieces.Piece, player int8) ([]pieces.Piece, bool) {
	// Returns if the action was successful and the current pieces on the indexed tile
	if !field.existsTile(index) {
		return nil, false
	}
	tile := field.GetTile(index)
	if tile.PlacedPieces[player] == piece {
		tile.RemovePiece(player)
		return tile.PlacedPieces, true
	}
	return nil, false
}

func (field *Field) GetPieceOnTile(index int16, player int8) pieces.Piece {
	tile, exists := field.tiles[index]
	if exists {
		return tile.PlacedPieces[player]
	}
	// If the tile does not exist, which means no piece was ever set here, return None
	return pieces.None
}

func (field *Field) CheckIfPlayerWon(lastPlacedIndex int16, player int8) *pieces.Line {
	// Returns the winning line if there is one, else returns nil
	// We only have to check the last placed piece and its surroundings, and if this created a winning line
	line := field.checkIfPlayerWonHorizontal(lastPlacedIndex, player)
	if line != nil {
		return line
	}
	line = field.checkIfPlayerWonVertical(lastPlacedIndex, player)
	if line != nil {
		return line
	}
	line = field.checkIfPlayerWonDiagonalLeftToRight(lastPlacedIndex, player)
	if line != nil {
		return line
	}
	line = field.checkIfPlayerWonDiagonalRightToLeft(lastPlacedIndex, player)
	if line != nil {
		return line
	}
	return nil
}

func (field *Field) GetNextWinningAction(lastPlacedIndex int16, player int8) map[int16][]pieces.Piece {
	// Checks if an action would lead directly to a win, given the last player's action
	// Return map of indexes of winning position and list of the winning pieces as its value

	winningActions := field.getHoriontalWinningActions(lastPlacedIndex, player)
	if len(winningActions) != 0 {
		return winningActions
	}
	winningActions = field.getVerticalWinningActions(lastPlacedIndex, player)
	if len(winningActions) != 0 {
		return winningActions
	}
	winningActions = field.getDiagonalLeftToRightWinningActions(lastPlacedIndex, player)
	if len(winningActions) != 0 {
		return winningActions
	}
	winningActions = field.getDiagonalRightToLeftWinningActions(lastPlacedIndex, player)
	if len(winningActions) != 0 {
		return winningActions
	}
	return nil
}

func (field *Field) getHoriontalWinningActions(lastPlacedIndex int16, player int8) map[int16][]pieces.Piece {
	lineBuilder := field.getHorizontalLineBuilder(lastPlacedIndex, player, true, true)
	return field.getWinningActionsFromLineBuilder(*lineBuilder)
}

func (field *Field) getVerticalWinningActions(lastPlacedIndex int16, player int8) map[int16][]pieces.Piece {
	lineBuilder := field.getVerticalLineBuilder(lastPlacedIndex, player, true, true)
	return field.getWinningActionsFromLineBuilder(*lineBuilder)
}

func (field *Field) getDiagonalLeftToRightWinningActions(lastPlacedIndex int16, player int8) map[int16][]pieces.Piece {
	lineBuilder := field.getDiagonalLeftToRightLineBuilder(lastPlacedIndex, player, true, true)
	return field.getWinningActionsFromLineBuilder(*lineBuilder)
}

func (field *Field) getDiagonalRightToLeftWinningActions(lastPlacedIndex int16, player int8) map[int16][]pieces.Piece {
	lineBuilder := field.getDiagonalRightToLeftLineBuilder(lastPlacedIndex, player, true, true)
	return field.getWinningActionsFromLineBuilder(*lineBuilder)
}

func (field *Field) getWinningActionsFromLineBuilder(lineBuilder LineBuilder) map[int16][]pieces.Piece {
	winningActions := map[int16][]pieces.Piece{}
	lines, options, optionIndexes := lineBuilder.GetLinesWithOptions()
	for i := 0; i < len(lines); i++ {
		if lines[i].ContainsWin() {
			winningActions[optionIndexes[i]] = append(winningActions[optionIndexes[i]], options[i])
		}
	}
	return winningActions
}

func (field *Field) GetLastIndex() int16 {
	return field.lastIndex
}

func (field *Field) existsTile(index int16) bool {
	_, exists := field.tiles[index]
	return exists
}

func (field *Field) checkIfPlayerWonHorizontal(lastPlacedIndex int16, player int8) *pieces.Line {
	lineBuilder := field.getHorizontalLineBuilder(lastPlacedIndex, player, false, false)
	line := lineBuilder.GetLine()
	if line.ContainsWin() {
		return line
	}
	return nil
}

func (field *Field) checkIfPlayerWonVertical(lastPlacedIndex int16, player int8) *pieces.Line {
	lineBuilder := field.getVerticalLineBuilder(lastPlacedIndex, player, false, false)
	line := lineBuilder.GetLine()
	if line.ContainsWin() {
		return line
	}
	return nil
}

func (field *Field) checkIfPlayerWonDiagonalLeftToRight(lastPlacedIndex int16, player int8) *pieces.Line {
	lineBuilder := field.getDiagonalLeftToRightLineBuilder(lastPlacedIndex, player, false, false)
	line := lineBuilder.GetLine()
	if line.ContainsWin() {
		return line
	}
	return nil
}

func (field *Field) checkIfPlayerWonDiagonalRightToLeft(lastPlacedIndex int16, player int8) *pieces.Line {
	lineBuilder := field.getDiagonalRightToLeftLineBuilder(lastPlacedIndex, player, false, false)
	line := lineBuilder.GetLine()
	if line.ContainsWin() {
		return line
	}
	return nil
}

func (field *Field) getHorizontalLineBuilder(lastPlacedIndex int16, player int8, useOptions, useEnemyPieces bool) *LineBuilder {
	lineBuilder := NewLineBuilder(field, useOptions, useEnemyPieces)
	// Go to the right, including the current index
	maxIndex := utils.GetMaxIndexRow(lastPlacedIndex, field.sizeX)
	for i := lastPlacedIndex; i <= maxIndex; i++ {
		if !lineBuilder.AddPieceRight(i, player) {
			break
		}
	}
	// Go to left
	minIndex := utils.GetMinIndexRow(lastPlacedIndex, field.sizeX)
	for i := lastPlacedIndex - 1; i >= minIndex; i-- {
		if !lineBuilder.AddPieceLeft(i, player) {
			break
		}
	}
	return lineBuilder
}

func (field *Field) getVerticalLineBuilder(lastPlacedIndex int16, player int8, useOptions, useEnemyPieces bool) *LineBuilder {
	lineBuilder := NewLineBuilder(field, useOptions, useEnemyPieces)
	// Go downwards, including the current index
	for i := lastPlacedIndex; i <= field.lastIndex; i += int16(field.sizeX) {
		if !lineBuilder.AddPieceRight(i, player) {
			break
		}
	}
	// Go upwards
	for i := lastPlacedIndex - int16(field.sizeX); i >= 0; i -= int16(field.sizeX) {
		if !lineBuilder.AddPieceLeft(i, player) {
			break
		}
	}
	return lineBuilder

}

func (field *Field) getDiagonalLeftToRightLineBuilder(lastPlacedIndex int16, player int8, useOptions, useEnemyPieces bool) *LineBuilder {
	lineBuilder := NewLineBuilder(field, useOptions, useEnemyPieces)
	// Go downwards and right, including the current index
	maxXIndex := utils.GetMaxIndexRow(lastPlacedIndex, field.sizeX)
	for i := lastPlacedIndex; i <= field.lastIndex && i%int16(field.sizeX) <= maxXIndex; i += (int16(field.sizeX) + 1) {
		if !lineBuilder.AddPieceRight(i, player) {
			break
		}
	}
	// Go upwards and left
	for i := lastPlacedIndex - int16(field.sizeX) - 1; i >= 0; i += (-int16(field.sizeX) - 1) {
		if !lineBuilder.AddPieceLeft(i, player) {
			break
		}
	}
	return lineBuilder
}

func (field *Field) getDiagonalRightToLeftLineBuilder(lastPlacedIndex int16, player int8, useOptions, useEnemyPieces bool) *LineBuilder {
	lineBuilder := NewLineBuilder(field, useOptions, useEnemyPieces)
	// Go upwards and right, including the current index
	maxXIndex := utils.GetMaxIndexRow(lastPlacedIndex, field.sizeX)
	for i := lastPlacedIndex; i >= 0 && i%int16(field.sizeX) <= maxXIndex; i += (-int16(field.sizeX) + 1) {
		if !lineBuilder.AddPieceRight(i, player) {
			break
		}
	}
	// Go downwards and left
	for i := lastPlacedIndex + int16(field.sizeX) - 1; i <= field.lastIndex && i%int16(field.sizeX) >= 0; i += (int16(field.sizeX) - 1) {
		if !lineBuilder.AddPieceLeft(i, player) {
			break
		}
	}
	return lineBuilder
}
