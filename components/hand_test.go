package components

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/natjo/interplay-bot/pieces"
)

func TestReduce(t *testing.T) {
	// Given
	hand := NewHand()

	// When
	hand.ReducePiecesLeft(pieces.Cylinder)
	hand.ReducePiecesLeft(pieces.Stick)
	hand.ReducePiecesLeft(pieces.Tube)

	// Then
	assert.EqualValues(t, pieces.InitPiecesStick-1, hand.piecesLeft[pieces.Stick])
	assert.EqualValues(t, pieces.InitPiecesTube-1, hand.piecesLeft[pieces.Tube])
	assert.EqualValues(t, pieces.InitPiecesCylinder-1, hand.piecesLeft[pieces.Cylinder])
}

func TestIncrease(t *testing.T) {
	// Given
	hand := NewHand()

	// When
	hand.IncreasePiecesLeft(pieces.Cylinder)
	hand.IncreasePiecesLeft(pieces.Stick)
	hand.IncreasePiecesLeft(pieces.Tube)

	// Then
	assert.EqualValues(t, pieces.InitPiecesStick+1, hand.piecesLeft[pieces.Stick])
	assert.EqualValues(t, pieces.InitPiecesTube+1, hand.piecesLeft[pieces.Tube])
	assert.EqualValues(t, pieces.InitPiecesCylinder+1, hand.piecesLeft[pieces.Cylinder])
}

func TestIsAllowedToRemoveStickSuccess(t *testing.T) {
	// Given
	hand := NewHand()
	hand.piecesLeft[pieces.Stick] = 0

	// When
	out := hand.IsAllowedToRemove(pieces.Stick)

	// Then
	assert.Equal(t, true, out)
}

func TestIsAllowedToRemoveStickFail(t *testing.T) {
	// Given
	hand := NewHand()
	hand.piecesLeft[pieces.Stick] = 1

	// When
	out := hand.IsAllowedToRemove(pieces.Stick)

	// Then
	assert.Equal(t, false, out)
}

func TestIsAllowedToRemoveTubeFail(t *testing.T) {
	// Given
	hand := NewHand()
	hand.piecesLeft[pieces.Tube] = 0
	hand.piecesLeft[pieces.Cylinder] = 1

	// When
	out := hand.IsAllowedToRemove(pieces.Tube)

	// Then
	assert.Equal(t, false, out)
}

func TestIsAllowedToRemoveTubeSuccess(t *testing.T) {
	// Given
	hand := NewHand()
	hand.piecesLeft[pieces.Tube] = 0
	hand.piecesLeft[pieces.Cylinder] = 0

	// When
	out := hand.IsAllowedToRemove(pieces.Tube)

	// Then
	assert.Equal(t, true, out)
}
