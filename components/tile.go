package components

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/interplay-bot/pieces"
	"gitlab.com/natjo/interplay-bot/utils"
)

type Tile struct {
	// Always a list of two, where the first entry is for player0 and the second entry is for player1
	PlacedPieces []pieces.Piece
}

func NewTile() *Tile {
	return &Tile{PlacedPieces: []pieces.Piece{pieces.None, pieces.None}}
}

func (t *Tile) Clone() *Tile {
	return &Tile{PlacedPieces: []pieces.Piece{t.PlacedPieces[0], t.PlacedPieces[1]}}
}

func (t *Tile) GetPiece(player int8) pieces.Piece {
	return t.PlacedPieces[player]
}

func (t *Tile) RemovePiece(player int8) {
	t.PlacedPieces[player] = pieces.None
}

func (t *Tile) SetPiece(piece pieces.Piece, player int8) bool {
	if !t.SetPieceAllowed(piece, player) {
		return false
	}
	t.PlacedPieces[player] = piece
	return true
}

func (t *Tile) SetPieceAllowed(piece pieces.Piece, player int8) bool {
	if t.PlacedPieces[player] != pieces.None {
		logrus.Debug("Player already has a piece on this tile")
		return false
	}
	otherPlayer := utils.GetOtherPlayer(player)
	if t.PlacedPieces[otherPlayer] == pieces.Cylinder {
		logrus.Debug("Other player already has a cylinder on this tile")
		return false
	}
	if piece == pieces.Cylinder && t.PlacedPieces[otherPlayer] != pieces.None {
		logrus.Debug("Other player already has a piece on this tile, Cylinder not allowed")
		return false
	}
	if t.PlacedPieces[otherPlayer] == piece {
		logrus.Debug(fmt.Sprintf("Other player already has a %v on this tile", pieces.PieceName(piece)))
		return false
	}
	return true
}
