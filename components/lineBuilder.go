package components

import (
	"gitlab.com/natjo/interplay-bot/pieces"
	"gitlab.com/natjo/interplay-bot/utils"
)

type LineBuilder struct {
	field                *Field
	lineL1               []pieces.Piece
	lineL0               []pieces.Piece
	lineR0               []pieces.Piece
	lineR1               []pieces.Piece
	optionsL             []pieces.Piece
	optionsR             []pieces.Piece
	optionsOnEnemyFields map[int16]pieces.Piece
	optionsIndexL        int16
	optionsIndexR        int16
	reversedLeftLines    bool
	useOptions           bool
	useEnemyPieces       bool
}

func NewLineBuilder(field *Field, useOptions bool, useEnemyPieces bool) *LineBuilder {
	// If useOptions is true, linebuilder should not only run until an empty field is found,
	// But to add an optional value to the first encounter of an empty field.
	// It sens a stop signal on the second encounter of an empty field.
	// If this is not set to true, the GetLinesWithOptions function will return only the center line, without optional lines

	// If useEnemyPieces is true, it will simulate played pieces where the enemy already placed one,
	// e.g. if the enemy has a tube, it will simulate a stick
	lb := &LineBuilder{}
	lb.field = field
	// -1 indicates it wasn't used yet
	lb.optionsIndexL = -1
	lb.optionsIndexR = -1
	lb.reversedLeftLines = false
	lb.useOptions = useOptions
	lb.useEnemyPieces = useEnemyPieces
	lb.optionsOnEnemyFields = map[int16]pieces.Piece{}
	return lb
}

func (lb *LineBuilder) AddPieceLeft(index int16, player int8) bool {
	// Returns false if there is no more elements in this direction
	currentPiece := lb.getPieceOnTile(index, player)
	if currentPiece == pieces.None {
		if lb.useOptions && !lb.leftOptionAlreadyFound() {
			lb.optionsL = lb.getOptions(index, player)
			lb.optionsIndexL = index
			return true
		} else {
			return false
		}
	}
	if !lb.leftOptionAlreadyFound() {
		lb.lineL0 = append(lb.lineL0, currentPiece)
	} else {
		lb.lineL1 = append(lb.lineL1, currentPiece)
	}
	return true
}

func (lb *LineBuilder) AddPieceRight(index int16, player int8) bool {
	// Returns false if there is no more elements in this direction
	currentPiece := lb.getPieceOnTile(index, player)
	if currentPiece == pieces.None {
		if lb.useOptions && !lb.rightOptionAlreadyFound() {
			lb.optionsR = lb.getOptions(index, player)
			lb.optionsIndexR = index
			return true
		} else {
			return false
		}
	}
	if !lb.rightOptionAlreadyFound() {
		lb.lineR0 = append(lb.lineR0, currentPiece)
	} else {
		lb.lineR1 = append(lb.lineR1, currentPiece)
	}
	return true
}

func (lb *LineBuilder) GetLine() *pieces.Line {
	lb.reverseLeftLines()
	lineM := append(lb.lineL0, lb.lineR0...)
	finalLine := pieces.NewLine(len(lineM))
	for _, p := range lineM {
		finalLine.Append(p)
	}
	return finalLine
}

func (lb *LineBuilder) GetLinesWithOptions() ([]*pieces.Line, []pieces.Piece, []int16) {
	lb.reverseLeftLines()

	lines := []*pieces.Line{}
	options := []pieces.Piece{}
	optionIndexes := []int16{}

	// Only center
	lineParts := [][]pieces.Piece{
		lb.lineL0,
		lb.lineR0,
	}
	finalLine := lb.buildLine(lineParts)
	for index, option := range lb.optionsOnEnemyFields {
		lines = append(lines, finalLine)
		options = append(options, option)
		optionIndexes = append(optionIndexes, index)
	}

	// Expand to left
	for _, option := range lb.optionsL {
		lineParts := [][]pieces.Piece{
			lb.lineL1,
			{option},
			lb.lineL0,
			lb.lineR0,
		}
		finalLine := lb.buildLine(lineParts)
		lines = append(lines, finalLine)
		options = append(options, option)
		optionIndexes = append(optionIndexes, lb.optionsIndexL)
	}

	// Expand to right
	for _, option := range lb.optionsR {
		lineParts := [][]pieces.Piece{
			lb.lineL0,
			lb.lineR0,
			{option},
			lb.lineR1,
		}
		finalLine := lb.buildLine(lineParts)
		lines = append(lines, finalLine)
		options = append(options, option)
		optionIndexes = append(optionIndexes, lb.optionsIndexR)
	}

	return lines, options, optionIndexes
}

func (lb *LineBuilder) getPieceOnTile(index int16, player int8) pieces.Piece {
	currentPiece := lb.field.GetPieceOnTile(index, player)
	if currentPiece != pieces.None || !lb.useEnemyPieces {
		return currentPiece
	}
	// Otherwise check enemy's piece on this field and return the counter piece to it
	otherPlayer := utils.GetOtherPlayer(player)
	currentPiece = lb.field.GetPieceOnTile(index, otherPlayer)
	if currentPiece == pieces.Stick {
		lb.optionsOnEnemyFields[index] = pieces.Tube
		return pieces.Tube
	}
	if currentPiece == pieces.Tube {
		lb.optionsOnEnemyFields[index] = pieces.Tube
		return pieces.Stick
	}
	return pieces.None
}

func (lb *LineBuilder) buildLine(lineParts [][]pieces.Piece) *pieces.Line {
	// First get the total length of the final build
	totalLength := 0
	for _, linePart := range lineParts {
		totalLength += len(linePart)
	}
	finalLine := pieces.NewLine(totalLength)
	for _, linePart := range lineParts {
		for _, piece := range linePart {
			finalLine.Append(piece)
		}
	}
	return finalLine
}

func (lb *LineBuilder) getOptions(index int16, player int8) []pieces.Piece {
	// TODO add actual options
	allOptions := []pieces.Piece{pieces.Tube, pieces.Cylinder, pieces.Stick}
	return allOptions
}

func (lb *LineBuilder) leftOptionAlreadyFound() bool {
	return lb.optionsIndexL != -1
}

func (lb *LineBuilder) rightOptionAlreadyFound() bool {
	return lb.optionsIndexR != -1
}

func (lb *LineBuilder) reverseLeftLines() {
	if !lb.reversedLeftLines {
		utils.ReverseSlice(lb.lineL1)
		utils.ReverseSlice(lb.lineL0)
		// We only reverse once, the change stays so we can treat it as non reverse built
		lb.reversedLeftLines = false
	}
}
