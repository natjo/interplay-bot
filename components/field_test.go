package components

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/natjo/interplay-bot/pieces"
)

func TestHorizontalWin(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(0, pieces.Stick, 0)
	field.SetPiece(1, pieces.Tube, 0)
	field.SetPiece(2, pieces.Cylinder, 0)
	field.SetPiece(3, pieces.Cylinder, 0)
	field.SetPiece(4, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(4, 0))
}

func TestHorizontalNotWin(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(0, pieces.Stick, 0)
	field.SetPiece(2, pieces.Stick, 0)
	field.SetPiece(3, pieces.Tube, 0)
	field.SetPiece(4, pieces.Stick, 0)

	// Then
	assert.Nil(t, field.CheckIfPlayerWon(4, 0))
}

func TestVerticalWin(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*0, pieces.Stick, 0)
	field.SetPiece(7*1, pieces.Tube, 0)
	field.SetPiece(7*2, pieces.Tube, 0)
	field.SetPiece(7*3, pieces.Tube, 0)
	field.SetPiece(7*4, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*4, 0))
}

func TestVertical2Win(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*2, pieces.Stick, 0)
	field.SetPiece(7*3, pieces.Tube, 0)
	field.SetPiece(7*4, pieces.Tube, 0)
	field.SetPiece(7*5, pieces.Tube, 0)
	field.SetPiece(7*6, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*6, 0))
}

func TestVertical3Win(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*0, pieces.Stick, 0)
	field.SetPiece(7*1, pieces.Stick, 0)
	field.SetPiece(7*2, pieces.Tube, 0)
	field.SetPiece(7*3, pieces.Stick, 0)
	field.SetPiece(7*4, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*3, 0))
}

func TestVerticalNoWin(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*0, pieces.Stick, 0)
	field.SetPiece(7*1, pieces.Stick, 0)
	field.SetPiece(7*2, pieces.Tube, 0)
	field.SetPiece(7*3, pieces.Stick, 0)
	field.SetPiece(7*6, pieces.Tube, 0)

	// Then
	assert.Nil(t, field.CheckIfPlayerWon(7*3, 0))
}

func TestDiagonalLeftToRightWin(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*0+0, pieces.Stick, 0)
	field.SetPiece(7*1+1, pieces.Tube, 0)
	field.SetPiece(7*2+2, pieces.Tube, 0)
	field.SetPiece(7*3+3, pieces.Tube, 0)
	field.SetPiece(7*4+4, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*4+4, 0))
}

func TestDiagonalLeftToRight2Win(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*0+0, pieces.Stick, 0)
	field.SetPiece(7*1+1, pieces.Tube, 0)
	field.SetPiece(7*2+2, pieces.Tube, 0)
	field.SetPiece(7*3+3, pieces.Tube, 0)
	field.SetPiece(7*4+4, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*0+0, 0))
}

func TestDiagonalLeftToRight3Win(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*0+2, pieces.Stick, 0)
	field.SetPiece(7*1+3, pieces.Tube, 0)
	field.SetPiece(7*2+4, pieces.Tube, 0)
	field.SetPiece(7*3+5, pieces.Tube, 0)
	field.SetPiece(7*4+6, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*4+6, 0))
}

func TestDiagonalRightToLeftWin(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*0+4, pieces.Stick, 0)
	field.SetPiece(7*1+3, pieces.Stick, 0)
	field.SetPiece(7*2+2, pieces.Tube, 0)
	field.SetPiece(7*3+1, pieces.Stick, 0)
	field.SetPiece(7*4+0, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*4+0, 0))
}

func TestDiagonaRightToLeft2Win(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*2+4, pieces.Stick, 0)
	field.SetPiece(7*3+3, pieces.Stick, 0)
	field.SetPiece(7*4+2, pieces.Tube, 0)
	field.SetPiece(7*5+1, pieces.Stick, 0)
	field.SetPiece(7*6+0, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*4+2, 0))
}

func TestDiagonalRightToLeft3Win(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	field.SetPiece(7*2+2, pieces.Stick, 0)
	field.SetPiece(7*3+3, pieces.Stick, 0)
	field.SetPiece(7*4+4, pieces.Tube, 0)
	field.SetPiece(7*5+5, pieces.Stick, 0)
	field.SetPiece(7*6+6, pieces.Stick, 0)

	// Then
	assert.NotNil(t, field.CheckIfPlayerWon(7*6+6, 0))
}

func TestLastIndex0(t *testing.T) {
	// Given
	field := NewField(1, 1)

	// When
	out := field.GetLastIndex()

	// Then
	assert.EqualValues(t, 0, out)
}

func TestLastIndex1(t *testing.T) {
	// Given
	field := NewField(2, 2)

	// When
	out := field.GetLastIndex()

	// Then
	assert.EqualValues(t, 3, out)
}

func TestLastIndex2(t *testing.T) {
	// Given
	field := NewField(7, 7)

	// When
	out := field.GetLastIndex()

	// Then
	assert.EqualValues(t, 48, out)
}

func TestSetPieceAllowed(t *testing.T) {
	type testParam struct {
		// Indexes of pieces and players belong together and together mean an option
		// We don't specify locations since we always act on the same index
		pieces  []pieces.Piece
		players []int8
		allowed bool
	}
	parameters := []testParam{
		{
			pieces:  []pieces.Piece{pieces.Stick, pieces.Tube},
			players: []int8{0, 1},
			allowed: true,
		},
		{
			pieces:  []pieces.Piece{pieces.Stick, pieces.Stick},
			players: []int8{0, 1},
			allowed: false,
		},
		{
			pieces:  []pieces.Piece{pieces.Stick, pieces.Cylinder},
			players: []int8{0, 0},
			allowed: false,
		},
	}
	for _, params := range parameters {
		// Given
		field := NewField(7, 7)

		// When
		field.SetPiece(0, params.pieces[0], params.players[0])
		allowed := field.SetPieceAllowed(0, params.pieces[1], params.players[1])

		// Then
		assert.Equal(t, params.allowed, allowed)
	}
}

func TestHorizontalWinActionDetected0(t *testing.T) {
	// Given
	field := NewField(7, 7)
	field.SetPiece(1, pieces.Stick, 0)
	field.SetPiece(2, pieces.Tube, 0)
	field.SetPiece(3, pieces.Stick, 0)
	field.SetPiece(5, pieces.Stick, 0)

	// When
	winningOptions := field.GetNextWinningAction(3, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube, pieces.Cylinder},
		winningOptions[4],
	)
}

func TestHorizontalWinActionDetectedWithEnemyTube(t *testing.T) {
	// Given
	field := NewField(7, 7)
	// High Low (Enemy Low) None High
	field.SetPiece(1, pieces.Stick, 0)
	field.SetPiece(2, pieces.Tube, 0)
	field.SetPiece(3, pieces.Tube, 1)
	field.SetPiece(5, pieces.Stick, 0)

	// When
	winningOptions := field.GetNextWinningAction(2, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube, pieces.Cylinder},
		winningOptions[4],
	)
}

func TestHorizontalWinActionDetectedWithEnemyStick(t *testing.T) {
	// Given
	field := NewField(7, 7)
	// High High (Enemy Stick) None High
	field.SetPiece(1, pieces.Stick, 0)
	field.SetPiece(2, pieces.Stick, 0)
	field.SetPiece(3, pieces.Stick, 1)
	field.SetPiece(5, pieces.Stick, 0)

	// When
	winningOptions := field.GetNextWinningAction(5, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Stick},
		winningOptions[4],
	)
}

func TestHorizontalNoWinActionDetectedWithEnemyTube(t *testing.T) {
	// Given
	field := NewField(7, 7)
	// High High (Enemy Tube) None High
	field.SetPiece(1, pieces.Stick, 0)
	field.SetPiece(2, pieces.Stick, 0)
	field.SetPiece(3, pieces.Tube, 1)
	field.SetPiece(5, pieces.Stick, 0)

	// When
	winningOptions := field.GetNextWinningAction(5, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{},
		winningOptions[4],
	)
}

func TestHorizontalWinActionDetected1(t *testing.T) {
	// Given
	field := NewField(7, 7)
	// Multiple ways to win:
	// - 31313
	// - 31323
	// - 33133
	// 3 3 1 3 X 3 3
	field.SetPiece(0, pieces.Stick, 0)
	field.SetPiece(1, pieces.Stick, 0)
	field.SetPiece(2, pieces.Tube, 0)
	field.SetPiece(3, pieces.Stick, 0)
	field.SetPiece(5, pieces.Stick, 0)
	field.SetPiece(6, pieces.Stick, 0)

	// When
	winningOptions := field.GetNextWinningAction(3, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube, pieces.Cylinder, pieces.Stick},
		winningOptions[4],
	)
}

func TestHorizontalWinActionDetectedEnemyCylinder(t *testing.T) {
	// Given
	field := NewField(7, 7)
	// 3 1 1 X 3 3
	field.SetPiece(1, pieces.Stick, 0)
	field.SetPiece(2, pieces.Tube, 0)
	field.SetPiece(3, pieces.Tube, 0)
	field.SetPiece(5, pieces.Stick, 0)
	field.SetPiece(6, pieces.Stick, 0)
	// Other player has a stick on winning position, no cylinder allowed here, only tube
	field.SetPiece(4, pieces.Stick, 1)

	// When
	winningOptions := field.GetNextWinningAction(3, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube},
		winningOptions[4],
	)
}

func TestHorizontalWinDetectedTwoOptions(t *testing.T) {
	// Given
	field := NewField(7, 7)
	// 3 X 3 1 3 X 3
	field.SetPiece(0, pieces.Stick, 0)
	field.SetPiece(2, pieces.Stick, 0)
	field.SetPiece(3, pieces.Tube, 0)
	field.SetPiece(4, pieces.Stick, 0)
	field.SetPiece(6, pieces.Stick, 0)

	// When
	winningOptions := field.GetNextWinningAction(3, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube, pieces.Cylinder},
		winningOptions[1],
	)

	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube, pieces.Cylinder},
		winningOptions[5],
	)
}

func TestVerticalWinDetectedTwoOptions(t *testing.T) {
	// Given
	field := NewField(7, 7)
	// 3 X 3 1 3 X 3
	field.SetPiece(7*0, pieces.Stick, 0)
	field.SetPiece(7*2, pieces.Stick, 0)
	field.SetPiece(7*3, pieces.Tube, 0)
	field.SetPiece(7*4, pieces.Stick, 0)
	field.SetPiece(7*6, pieces.Stick, 0)

	// When
	winningOptions := field.GetNextWinningAction(7*3, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube, pieces.Cylinder},
		winningOptions[7*1],
	)

	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube, pieces.Cylinder},
		winningOptions[7*5],
	)
}

func TestHorizontalWinActionDetectedEnemyOnAllPieces(t *testing.T) {
	// Given
	field := NewField(7, 7)
	// Multiple ways to win:
	// - 3X3X3
	// where on each X the enemy already placed a stick
	field.SetPiece(0, pieces.Stick, 0)
	field.SetPiece(1, pieces.Stick, 1)
	field.SetPiece(2, pieces.Stick, 0)
	field.SetPiece(3, pieces.Stick, 1)
	field.SetPiece(4, pieces.Stick, 0)

	// When
	winningOptions := field.GetNextWinningAction(4, 0)

	// Then
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube},
		winningOptions[1],
	)
	assert.ElementsMatch(
		t,
		[]pieces.Piece{pieces.Tube},
		winningOptions[3],
	)
}
