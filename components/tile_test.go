package components

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/natjo/interplay-bot/pieces"
)

func TestSetPiece(t *testing.T) {
	type testParam struct {
		originalPieces  []pieces.Piece
		resultingPieces []pieces.Piece
		newPiece        pieces.Piece
		player          int8
		allowed         bool
	}
	parameters := []testParam{
		{
			originalPieces:  []pieces.Piece{pieces.None, pieces.None},
			resultingPieces: []pieces.Piece{pieces.Stick, pieces.None},
			newPiece:        pieces.Stick,
			player:          0,
			allowed:         true},
		{
			originalPieces:  []pieces.Piece{pieces.None, pieces.None},
			resultingPieces: []pieces.Piece{pieces.None, pieces.Tube},
			newPiece:        pieces.Tube,
			player:          1,
			allowed:         true},
		{
			originalPieces:  []pieces.Piece{pieces.Tube, pieces.None},
			resultingPieces: []pieces.Piece{pieces.Tube, pieces.Stick},
			newPiece:        pieces.Stick,
			player:          1,
			allowed:         true},
		{
			originalPieces:  []pieces.Piece{pieces.None, pieces.Tube},
			resultingPieces: []pieces.Piece{pieces.None, pieces.Tube},
			newPiece:        pieces.Tube,
			player:          1,
			allowed:         false},
		{
			originalPieces:  []pieces.Piece{pieces.Stick, pieces.None},
			resultingPieces: []pieces.Piece{pieces.Stick, pieces.None},
			newPiece:        pieces.Stick,
			player:          1,
			allowed:         false},
		{
			originalPieces:  []pieces.Piece{pieces.Cylinder, pieces.None},
			resultingPieces: []pieces.Piece{pieces.Cylinder, pieces.None},
			newPiece:        pieces.Stick,
			player:          1,
			allowed:         false},
		{
			originalPieces:  []pieces.Piece{pieces.None, pieces.Tube},
			resultingPieces: []pieces.Piece{pieces.None, pieces.Tube},
			newPiece:        pieces.Cylinder,
			player:          0,
			allowed:         false},
		{
			originalPieces:  []pieces.Piece{pieces.None, pieces.Tube},
			resultingPieces: []pieces.Piece{pieces.None, pieces.Tube},
			newPiece:        pieces.Tube,
			player:          0,
			allowed:         false},
		{
			originalPieces:  []pieces.Piece{pieces.None, pieces.Stick},
			resultingPieces: []pieces.Piece{pieces.None, pieces.Stick},
			newPiece:        pieces.Cylinder,
			player:          0,
			allowed:         false},
	}
	for _, params := range parameters {
		//Given
		tile := NewTile()
		tile.PlacedPieces = params.originalPieces

		// When
		allowed := tile.SetPiece(params.newPiece, params.player)

		// Then
		assert.Equal(t, params.resultingPieces, tile.PlacedPieces)
		assert.Equal(t, params.allowed, allowed)
	}
}
