package components

import "gitlab.com/natjo/interplay-bot/pieces"

type Hand struct {
	piecesLeft map[pieces.Piece]int8
}

func NewHand() *Hand {
	return &Hand{
		piecesLeft: pieces.GetInitPieces(),
	}
}

func (hand *Hand) Clone() *Hand {
	clonedHand := &Hand{
		piecesLeft: map[pieces.Piece]int8{},
	}
	// Clone map of pieces left
	for k, v := range hand.piecesLeft {
		clonedHand.piecesLeft[k] = v
	}
	return clonedHand
}

func (hand *Hand) ReducePiecesLeft(piece pieces.Piece) {
	// Always decreases by 1, since that's the maximum that can be done per action
	hand.piecesLeft[piece] -= 1
}

func (hand *Hand) IncreasePiecesLeft(piece pieces.Piece) {
	// Always increases by 1, since that's the maximum that can be done per action
	hand.piecesLeft[piece] += 1
}

func (hand *Hand) GetPiecesLeft() map[pieces.Piece]int8 {
	// This struct is just a wrapper around the count of each type of piece.
	// Sometimes we do need direct access to the pieces
	return hand.piecesLeft
}

func (hand *Hand) IsAllowedToRemove(piece pieces.Piece) bool {
	// The player is allowed to pickup a stick if they run out of sticks, but only a cylinder/tube once he run out of both
	if piece == pieces.Stick {
		return hand.piecesLeft[pieces.Stick] == 0
	}
	return hand.piecesLeft[pieces.Cylinder] == 0 && hand.piecesLeft[pieces.Tube] == 0
}

func (hand *Hand) IsPieceAvailable(piece pieces.Piece) bool {
	return hand.piecesLeft[piece] > 0
}
