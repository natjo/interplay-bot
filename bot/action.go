package bot

import (
	mcts "gitlab.com/natjo/gomcts"
	"gitlab.com/natjo/interplay-bot/pieces"
)

type Action struct {
	Index  int16
	Piece  pieces.Piece
	Player int8
}

func NewAction(index int16, piece pieces.Piece, player int8) *Action {
	return &Action{
		Index:  index,
		Piece:  piece,
		Player: player,
	}
}

func (action *Action) GetActor() int {
	return int(action.Player)
}

func (action *Action) IsEqual(a mcts.Action) bool {
	otherAction := a.(*Action)
	return action.Index == otherAction.Index &&
		action.Piece == otherAction.Piece &&
		action.Player == otherAction.Player
}
