package bot

import (
	"time"

	"github.com/sirupsen/logrus"
	mcts "gitlab.com/natjo/gomcts"
	"gitlab.com/natjo/interplay-bot/pieces"
)

type Bot struct {
	mctsTree *mcts.Tree
}

func NewBot(sizeX, sizeY, startingPlayer int8) *Bot {
	state := NewState(sizeX, sizeY, startingPlayer)
	searchDepth := 40
	explorationTime := time.Second * 6
	threads := 20
	explorationParameter := 4.44

	mctsTree := mcts.NewTree(state, searchDepth, int(startingPlayer), explorationTime)
	mctsTree.SetThreads(threads)
	mctsTree.SetPrintLogs(true)
	mctsTree.SetExplorationParameter(explorationParameter)

	return &Bot{mctsTree: mctsTree}
}

func (bot *Bot) MakeDecision() (int16, pieces.Piece) {
	// Returns the decision the bot made
	// index and piece it chose
	logrus.Info("Bot is making new decision")
	bot.mctsTree.StartExplore()
	action, ok := bot.mctsTree.GetBestAction().(*Action)
	if !ok {
		logrus.Panic("Failed to understand response of bot")
	}
	return action.Index, action.Piece
}

func (bot *Bot) UpdateFromAction(index int16, piece pieces.Piece, player int8) {
	action := NewAction(index, piece, player)
	bot.mctsTree.UpdateFromAction(action)
}
