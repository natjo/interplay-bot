package bot

import (
	mcts "gitlab.com/natjo/gomcts"
	"gitlab.com/natjo/interplay-bot/components"
	"gitlab.com/natjo/interplay-bot/pieces"
	"gitlab.com/natjo/interplay-bot/utils"
)

// On `piecesPlayerMustPlay: If a player picks up a piece he has to immediately place it again.
// This fields allows tracking this throught action turns
// On `lastPlayedIndex` each entry of this field corresponds to the correct player
type State struct {
	field               *components.Field
	hands               []*components.Hand
	piecePlayerMustPlay []pieces.Piece
	sizeX               int8
	sizeY               int8
	currentPlayer       int8
	lastPlayedIndexes   []int16
}

func NewState(sizeX, sizeY int8, startingPlayer int8) *State {
	return &State{
		sizeX:               sizeX,
		sizeY:               sizeY,
		field:               components.NewField(sizeX, sizeY),
		hands:               []*components.Hand{components.NewHand(), components.NewHand()},
		piecePlayerMustPlay: []pieces.Piece{pieces.None, pieces.None},
		currentPlayer:       startingPlayer,
		lastPlayedIndexes:   []int16{0, 0},
	}
}

func (state *State) Clone() mcts.State {
	return &State{
		sizeX:               state.sizeX,
		sizeY:               state.sizeY,
		field:               state.field.Clone(),
		hands:               []*components.Hand{state.hands[0].Clone(), state.hands[1].Clone()},
		piecePlayerMustPlay: []pieces.Piece{state.piecePlayerMustPlay[0], state.piecePlayerMustPlay[1]},
		currentPlayer:       state.currentPlayer,
		lastPlayedIndexes:   state.lastPlayedIndexes,
	}
}

func (state *State) Apply(a mcts.Action) mcts.Outcome {
	// Conversion from interface to actual type in this module is always required
	action := a.(*Action)
	state.UpdateFromAction(action.Index, action.Piece, action.Player)

	// We only have to check for a win. Since an action can never cause a loss in the same turn
	// and there is no tie in this game
	if state.field.CheckIfPlayerWon(action.Index, action.Player) != nil {
		return mcts.Win
	}
	return mcts.NotFinal
}

func (state *State) UpdateFromAction(index int16, piece pieces.Piece, player int8) {
	// Updates the bots game state given the action, it always asumes the action is correct
	// While the bot stores the current player, we replace the stored value with the provided value here
	_, tileWasFree := state.field.SetPiece(index, piece, player)
	if tileWasFree {
		// Place a stone
		if state.piecePlayerMustPlay[player] == piece {
			state.piecePlayerMustPlay[player] = pieces.None
		} else {
			state.hands[player].ReducePiecesLeft(piece)
		}
		state.currentPlayer = utils.GetOtherPlayer(player)
	} else {
		// Pick up own stone
		state.field.RemovePiece(index, piece, player)
		state.piecePlayerMustPlay[player] = piece
		state.currentPlayer = player
	}

	// Update history of played indexes
	state.lastPlayedIndexes[player] = index
}

func (state *State) GetNextActions() []mcts.Action {
	player := state.currentPlayer

	// First check if a winning action is possible, if so return this
	winningActions := state.getWinningActions(player)

	if len(winningActions) > 0 {
		return winningActions
	}
	if state.piecePlayerMustPlay[player] != pieces.None {
		return state.getActionsPiecePlayerMustPlay(player)
	}
	return state.getActionsPieceFromHand(player)
}

func (state *State) getWinningActions(player int8) []mcts.Action {
	lastPlayerIndex := state.lastPlayedIndexes[player]
	winningAction := state.field.GetNextWinningAction(lastPlayerIndex, player)

	availablePieces := []pieces.Piece{state.piecePlayerMustPlay[player]}
	if state.piecePlayerMustPlay[player] == pieces.None {
		availablePieces = state.getAvailablePieces(player)
	}

	winningActions := []mcts.Action{}
	for index, pieces := range winningAction {
		for _, winningPiece := range pieces {
			for _, availablePiece := range availablePieces {
				if winningPiece == availablePiece {
					newWinningAction := NewAction(index, winningPiece, player)
					winningActions = append(winningActions, newWinningAction)
				}
			}
		}
	}
	return winningActions
}

func (state *State) getActionsPiecePlayerMustPlay(player int8) []mcts.Action {
	// Player picked something up last turn
	allActions := []mcts.Action{}
	piece := state.piecePlayerMustPlay[player]
	for i := int16(0); i <= state.field.GetLastIndex(); i++ {
		if state.field.SetPieceAllowed(i, piece, player) {
			allActions = append(allActions, NewAction(i, piece, player))
		}
	}
	return allActions
}

func (state *State) getActionsPieceFromHand(player int8) []mcts.Action {
	allActions := []mcts.Action{}
	availablePieces := state.getAvailablePieces(player)
	notAvailablePieces := state.getNotAvailablePieces(player)

	for i := int16(0); i <= state.field.GetLastIndex(); i++ {
		for _, piece := range availablePieces {
			if state.field.SetPieceAllowed(i, piece, player) {
				allActions = append(allActions, NewAction(i, piece, player))
			}
		}
		for _, piece := range notAvailablePieces {
			if state.field.RemovePieceAllowed(i, piece, player) {
				allActions = append(allActions, NewAction(i, piece, player))
			}
		}
	}
	return allActions
}

func (state *State) getAvailablePieces(player int8) []pieces.Piece {
	allPieces := []pieces.Piece{pieces.Tube, pieces.Cylinder, pieces.Stick}
	availablePieces := []pieces.Piece{}
	for _, piece := range allPieces {
		if state.hands[player].IsPieceAvailable(piece) {
			availablePieces = append(availablePieces, piece)
		}
	}
	return availablePieces
}

func (state *State) getNotAvailablePieces(player int8) []pieces.Piece {
	allPieces := []pieces.Piece{pieces.Tube, pieces.Cylinder, pieces.Stick}
	availablePieces := []pieces.Piece{}
	for _, piece := range allPieces {
		if !state.hands[player].IsPieceAvailable(piece) {
			availablePieces = append(availablePieces, piece)
		}
	}
	return availablePieces
}

func (state *State) nextPlayer() {
	state.currentPlayer = utils.GetOtherPlayer(state.currentPlayer)
}
