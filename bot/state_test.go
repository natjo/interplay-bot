package bot

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/natjo/interplay-bot/pieces"
)

func TestPlayCylinder(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)
	currentPlayer := int8(0)
	index := int16(0)
	piece := pieces.Cylinder

	// When
	state.UpdateFromAction(index, piece, currentPlayer)

	// Then
	assert.Equal(t, piece, state.field.GetPieceOnTile(index, currentPlayer))
	assert.EqualValues(t, pieces.InitPiecesCylinder-1, state.hands[currentPlayer].GetPiecesLeft()[piece])
}

func TestPickUpCylinder(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)
	currentPlayer := int8(0)
	// The player has all 4 cylinder pieces played
	state.UpdateFromAction(0, pieces.Cylinder, currentPlayer)
	state.UpdateFromAction(1, pieces.Cylinder, currentPlayer)
	state.UpdateFromAction(2, pieces.Cylinder, currentPlayer)
	state.UpdateFromAction(3, pieces.Cylinder, currentPlayer)
	state.UpdateFromAction(4, pieces.Tube, currentPlayer)
	state.UpdateFromAction(5, pieces.Tube, currentPlayer)
	state.UpdateFromAction(6, pieces.Tube, currentPlayer)
	state.UpdateFromAction(7, pieces.Tube, currentPlayer)

	// When
	state.UpdateFromAction(0, pieces.Cylinder, currentPlayer)

	// Then
	assert.Equal(t, pieces.None, state.field.GetPieceOnTile(0, currentPlayer))
	assert.Equal(t, pieces.Cylinder, state.piecePlayerMustPlay[currentPlayer])
	assert.EqualValues(t, 0, state.hands[currentPlayer].GetPiecesLeft()[pieces.Tube])
	assert.EqualValues(t, 0, state.hands[currentPlayer].GetPiecesLeft()[pieces.Cylinder])
}

func TestPlayPickedUpStick(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)
	currentPlayer := int8(0)
	// The player has all stick pieces played
	state.UpdateFromAction(0, pieces.Stick, currentPlayer)
	state.UpdateFromAction(1, pieces.Stick, currentPlayer)
	state.UpdateFromAction(2, pieces.Stick, currentPlayer)
	state.UpdateFromAction(3, pieces.Stick, currentPlayer)
	state.UpdateFromAction(4, pieces.Stick, currentPlayer)
	state.UpdateFromAction(5, pieces.Stick, currentPlayer)
	state.UpdateFromAction(6, pieces.Stick, currentPlayer)
	state.UpdateFromAction(7, pieces.Stick, currentPlayer)
	state.UpdateFromAction(8, pieces.Stick, currentPlayer)
	state.UpdateFromAction(9, pieces.Stick, currentPlayer)
	// Pick up stick
	state.UpdateFromAction(0, pieces.Stick, currentPlayer)

	// When
	state.UpdateFromAction(0, pieces.Stick, currentPlayer)

	// Then
	assert.Equal(t, pieces.Stick, state.field.GetPieceOnTile(0, currentPlayer))
	assert.EqualValues(t, 0, state.hands[currentPlayer].GetPiecesLeft()[pieces.Stick])
	assert.Equal(t, pieces.None, state.piecePlayerMustPlay[currentPlayer])
}

func TestRandomMovesBothPlayers(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(0, pieces.Tube, 1)
	state.UpdateFromAction(20, pieces.Cylinder, 0)
	state.UpdateFromAction(13, pieces.Tube, 1)
	state.UpdateFromAction(4, pieces.Stick, 0)
	state.UpdateFromAction(8, pieces.Tube, 1)
	state.UpdateFromAction(60, pieces.Stick, 0)

	// Then
	assert.Equal(t, pieces.Stick, state.field.GetPieceOnTile(0, 0))
	assert.Equal(t, pieces.Tube, state.field.GetPieceOnTile(0, 1))
	assert.Equal(t, pieces.Stick, state.field.GetPieceOnTile(4, 0))
	assert.Equal(t, pieces.None, state.field.GetPieceOnTile(4, 1))
	assert.EqualValues(t, pieces.InitPiecesStick-3, state.hands[0].GetPiecesLeft()[pieces.Stick])
	assert.EqualValues(t, pieces.InitPiecesTube-3, state.hands[1].GetPiecesLeft()[pieces.Tube])
	assert.Equal(t, pieces.None, state.piecePlayerMustPlay[0])
	assert.Equal(t, pieces.None, state.piecePlayerMustPlay[1])
}

func TestGetActionsFirstAction(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	actions := state.GetNextActions()

	// Then
	assert.Len(t, actions, 7*7*3)
	assert.EqualValues(t, state.currentPlayer, 0)
}

func TestGetActionsAfterFirstActionStick(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Stick, 0)
	actions := state.GetNextActions()

	// Then
	assert.Len(t, actions, 7*7*3-2)
	assert.EqualValues(t, state.currentPlayer, 1)
}

func TestGetActionsAfterFirstActionTube(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Tube, 0)
	actions := state.GetNextActions()

	// Then
	assert.Len(t, actions, 7*7*3-2)
	assert.EqualValues(t, state.currentPlayer, 1)
}

func TestGetActionsAfterFirstActionCylinder(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Cylinder, 0)
	actions := state.GetNextActions()

	// Then
	assert.Len(t, actions, 7*7*3-3)
	assert.EqualValues(t, state.currentPlayer, 1)
}

func TestGetActionsSecondAction(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(17, pieces.Tube, 1)
	actions := state.GetNextActions()

	// Then
	lostOptions := 3 + 2
	assert.Len(t, actions, 7*7*3-lostOptions)
	assert.EqualValues(t, state.currentPlayer, 0)
}

func TestGetActionsThirdAction(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(17, pieces.Tube, 1)
	state.UpdateFromAction(20, pieces.Cylinder, 0)
	actions := state.GetNextActions()

	// Then
	lostOptions := 3 + 2 + 3
	assert.Len(t, actions, 7*7*3-lostOptions)
	assert.EqualValues(t, state.currentPlayer, 1)
}

func TestGetActionsFourthAction(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Stick, 1)
	state.UpdateFromAction(17, pieces.Tube, 0)
	state.UpdateFromAction(20, pieces.Cylinder, 1)
	state.UpdateFromAction(30, pieces.Stick, 0)
	actions := state.GetNextActions()

	// Then
	lostOptions := 3 + 2 + 3 + 2
	assert.Len(t, actions, 7*7*3-lostOptions)
	assert.EqualValues(t, state.currentPlayer, 1)
}

func TestGetActionsAllSticksPlayed(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(10, pieces.Stick, 1)
	state.UpdateFromAction(1, pieces.Stick, 0)
	state.UpdateFromAction(11, pieces.Stick, 1)
	state.UpdateFromAction(2, pieces.Stick, 0)
	state.UpdateFromAction(12, pieces.Stick, 1)
	state.UpdateFromAction(3, pieces.Stick, 0)
	state.UpdateFromAction(13, pieces.Stick, 1)
	state.UpdateFromAction(4, pieces.Stick, 0)
	state.UpdateFromAction(14, pieces.Stick, 1)
	state.UpdateFromAction(5, pieces.Stick, 0)
	state.UpdateFromAction(15, pieces.Stick, 1)
	state.UpdateFromAction(6, pieces.Stick, 0)
	state.UpdateFromAction(16, pieces.Stick, 1)
	state.UpdateFromAction(7, pieces.Stick, 0)
	state.UpdateFromAction(17, pieces.Stick, 1)
	state.UpdateFromAction(8, pieces.Stick, 0)
	state.UpdateFromAction(18, pieces.Stick, 1)
	state.UpdateFromAction(9, pieces.Stick, 0)
	state.UpdateFromAction(19, pieces.Stick, 1)
	actions := state.GetNextActions()

	// Then
	// Only times 2 because we are out of sticks
	// -10 because on 10 fields we can set tubes, but no cylinders
	// +10 because of pick up actions
	assert.Len(t, actions, 7*7*2-10*2-10+10)
	assert.EqualValues(t, state.currentPlayer, 0)
}

func TestGetActionsAllHighPlayed(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	state.UpdateFromAction(0, pieces.Tube, 0)
	state.UpdateFromAction(10, pieces.Stick, 1)
	state.UpdateFromAction(1, pieces.Tube, 0)
	state.UpdateFromAction(11, pieces.Stick, 1)
	state.UpdateFromAction(2, pieces.Tube, 0)
	state.UpdateFromAction(12, pieces.Stick, 1)
	state.UpdateFromAction(3, pieces.Tube, 0)
	state.UpdateFromAction(13, pieces.Stick, 1)
	state.UpdateFromAction(4, pieces.Cylinder, 0)
	state.UpdateFromAction(14, pieces.Stick, 1)
	state.UpdateFromAction(5, pieces.Cylinder, 0)
	state.UpdateFromAction(15, pieces.Stick, 1)
	state.UpdateFromAction(6, pieces.Cylinder, 0)
	state.UpdateFromAction(16, pieces.Stick, 1)
	state.UpdateFromAction(7, pieces.Cylinder, 0)
	state.UpdateFromAction(17, pieces.Stick, 1)

	actions := state.GetNextActions()

	// Then
	// Only times 1 because we only have sticks
	// -8 because we can't place sticks on already used fields
	// -8 because we can't place sticks on opponent's cylinders
	// +8 because of pick up actions
	assert.Len(t, actions, 7*7*1-8-8+8)
	assert.EqualValues(t, state.currentPlayer, 0)
}

func TestGetActionsStickPickedUp(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	// Place all sticks
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(10, pieces.Stick, 1)
	state.UpdateFromAction(1, pieces.Stick, 0)
	state.UpdateFromAction(11, pieces.Stick, 1)
	state.UpdateFromAction(2, pieces.Stick, 0)
	state.UpdateFromAction(12, pieces.Stick, 1)
	state.UpdateFromAction(3, pieces.Stick, 0)
	state.UpdateFromAction(13, pieces.Stick, 1)
	state.UpdateFromAction(4, pieces.Stick, 0)
	state.UpdateFromAction(14, pieces.Stick, 1)
	state.UpdateFromAction(5, pieces.Stick, 0)
	state.UpdateFromAction(15, pieces.Stick, 1)
	state.UpdateFromAction(6, pieces.Stick, 0)
	state.UpdateFromAction(16, pieces.Stick, 1)
	state.UpdateFromAction(7, pieces.Stick, 0)
	state.UpdateFromAction(17, pieces.Stick, 1)
	state.UpdateFromAction(8, pieces.Stick, 0)
	state.UpdateFromAction(18, pieces.Stick, 1)
	state.UpdateFromAction(9, pieces.Stick, 0)
	state.UpdateFromAction(19, pieces.Stick, 1)
	// Pick up stick
	state.UpdateFromAction(0, pieces.Stick, 0)
	actions := state.GetNextActions()

	// Then
	// Only placing stick allowed
	// -9 because of fields where sticks already are
	// -10 because of opponent's sticks
	assert.Len(t, actions, 7*7*1-9-10)
	assert.EqualValues(t, state.currentPlayer, 0)
}

func TestGetActionsTubePickedUp(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	// Place all tubes and cylinders
	state.UpdateFromAction(0, pieces.Tube, 0)
	state.UpdateFromAction(10, pieces.Stick, 1)
	state.UpdateFromAction(1, pieces.Tube, 0)
	state.UpdateFromAction(11, pieces.Stick, 1)
	state.UpdateFromAction(2, pieces.Tube, 0)
	state.UpdateFromAction(12, pieces.Stick, 1)
	state.UpdateFromAction(3, pieces.Tube, 0)
	state.UpdateFromAction(13, pieces.Stick, 1)
	state.UpdateFromAction(4, pieces.Cylinder, 0)
	state.UpdateFromAction(14, pieces.Stick, 1)
	state.UpdateFromAction(5, pieces.Cylinder, 0)
	state.UpdateFromAction(15, pieces.Stick, 1)
	state.UpdateFromAction(6, pieces.Cylinder, 0)
	state.UpdateFromAction(16, pieces.Stick, 1)
	state.UpdateFromAction(7, pieces.Cylinder, 0)
	state.UpdateFromAction(17, pieces.Stick, 1)
	// Pick up tube
	state.UpdateFromAction(0, pieces.Tube, 0)
	actions := state.GetNextActions()

	// Then
	// Only placing Tube allowed,
	// - 7 fields where tube/cylinder already are
	assert.Len(t, actions, 7*7*1-7)
	assert.EqualValues(t, state.currentPlayer, 0)
}

func TestGetActionsCylinderPickedUp(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	// Place all tubes and cylinders
	state.UpdateFromAction(0, pieces.Tube, 0)
	state.UpdateFromAction(10, pieces.Stick, 1)
	state.UpdateFromAction(1, pieces.Tube, 0)
	state.UpdateFromAction(11, pieces.Stick, 1)
	state.UpdateFromAction(2, pieces.Tube, 0)
	state.UpdateFromAction(12, pieces.Stick, 1)
	state.UpdateFromAction(3, pieces.Tube, 0)
	state.UpdateFromAction(13, pieces.Stick, 1)
	state.UpdateFromAction(4, pieces.Cylinder, 0)
	state.UpdateFromAction(14, pieces.Stick, 1)
	state.UpdateFromAction(5, pieces.Cylinder, 0)
	state.UpdateFromAction(15, pieces.Stick, 1)
	state.UpdateFromAction(6, pieces.Cylinder, 0)
	state.UpdateFromAction(16, pieces.Stick, 1)
	state.UpdateFromAction(7, pieces.Cylinder, 0)
	state.UpdateFromAction(17, pieces.Stick, 1)
	// Pick up cylinder
	state.UpdateFromAction(7, pieces.Cylinder, 0)
	actions := state.GetNextActions()

	// Then
	// Only placing Tube allowed,
	// - 7 fields where tube/cylinder already are
	// - 8 because of oppoenent's sticks
	assert.Len(t, actions, 7*7*1-7-8)
	assert.EqualValues(t, state.currentPlayer, 0)
	assert.ElementsMatch(t, state.lastPlayedIndexes, []int16{7, 17})
}

func TestGetActionsAllCylinderPlayed(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	// Place cylinders
	state.UpdateFromAction(4, pieces.Cylinder, 0)
	state.UpdateFromAction(10, pieces.Tube, 1)
	state.UpdateFromAction(5, pieces.Cylinder, 0)
	state.UpdateFromAction(11, pieces.Tube, 1)
	state.UpdateFromAction(6, pieces.Cylinder, 0)
	state.UpdateFromAction(12, pieces.Tube, 1)
	state.UpdateFromAction(7, pieces.Cylinder, 0)
	state.UpdateFromAction(13, pieces.Tube, 1)
	actions := state.GetNextActions()

	// Then
	// Only placing Cylinder allowed
	// - 4 fields where cylinder already are
	// - 4 fields where opponent's cylinders are
	assert.Len(t, actions, 7*7*2-4-4)
	assert.EqualValues(t, state.currentPlayer, 0)
	assert.ElementsMatch(t, state.lastPlayedIndexes, []int16{7, 13})
}

func TestLastPlayedIndex(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	// Place cylinders
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(1, pieces.Stick, 1)
	state.UpdateFromAction(2, pieces.Stick, 0)
	state.UpdateFromAction(3, pieces.Stick, 1)
	state.UpdateFromAction(4, pieces.Stick, 0)
	state.UpdateFromAction(5, pieces.Stick, 1)

	// Then
	assert.ElementsMatch(t, state.lastPlayedIndexes, []int16{4, 5})
}

func TestRandom(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	// When
	// Place cylinders
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(0, pieces.Tube, 1)
	state.UpdateFromAction(1, pieces.Stick, 0)
	actions := state.GetNextActions()

	// Then
	assert.ElementsMatch(t, state.lastPlayedIndexes, []int16{1, 0})
	assert.EqualValues(t, actions[0].(*Action).Player, 1)
	assert.EqualValues(t, state.currentPlayer, 1)
}

func TestWinningMovePossible(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)

	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(1, pieces.Cylinder, 0)
	state.UpdateFromAction(2, pieces.Cylinder, 0)
	state.UpdateFromAction(3, pieces.Cylinder, 0)
	state.UpdateFromAction(7, pieces.Stick, 1)
	state.UpdateFromAction(8, pieces.Stick, 1)
	state.UpdateFromAction(9, pieces.Stick, 1)
	state.UpdateFromAction(10, pieces.Stick, 1)

	// When
	actions := state.GetNextActions()

	// Then
	assert.Equal(t, 1, len(actions))
	assert.Equal(t, pieces.Stick, actions[0].(*Action).Piece)
	assert.EqualValues(t, 4, actions[0].(*Action).Index)
}

func TestWinningMovePossibleNoTube(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)
	// Get rid of all tubes so player must play cylinder
	state.UpdateFromAction(30, pieces.Tube, 0)
	state.UpdateFromAction(31, pieces.Tube, 0)
	state.UpdateFromAction(32, pieces.Tube, 0)
	state.UpdateFromAction(33, pieces.Tube, 0)
	// Prepare winning action
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(1, pieces.Cylinder, 0)
	state.UpdateFromAction(2, pieces.Cylinder, 0)
	state.UpdateFromAction(4, pieces.Stick, 0)
	// Just so it's player 0 turn again
	state.UpdateFromAction(6, pieces.Stick, 1)

	// When
	actions := state.GetNextActions()

	// Then
	assert.Equal(t, 1, len(actions))
	assert.Equal(t, pieces.Cylinder, actions[0].(*Action).Piece)
	assert.EqualValues(t, 3, actions[0].(*Action).Index)
}

func TestWinningMovePossibleMustPlayCylinder(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)
	// Get rid of all tubes so player must play cylinder
	state.UpdateFromAction(30, pieces.Cylinder, 0)
	state.UpdateFromAction(5, pieces.Cylinder, 0)
	// Prepare winning action
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(1, pieces.Cylinder, 0)
	state.UpdateFromAction(2, pieces.Cylinder, 0)
	state.UpdateFromAction(4, pieces.Stick, 0)
	// Pick up cylinder again, so it must be played
	state.UpdateFromAction(5, pieces.Cylinder, 0)
	// Just so it's player 0 turn again
	state.UpdateFromAction(6, pieces.Stick, 1)

	// When
	actions := state.GetNextActions()

	// Then
	assert.Equal(t, 1, len(actions))
	assert.Equal(t, pieces.Cylinder, actions[0].(*Action).Piece)
	assert.EqualValues(t, 3, actions[0].(*Action).Index)
}

func TestWinningMoveNotPossibleNoStick(t *testing.T) {
	// Given
	state := NewState(7, 7, 0)
	// Get rid of all tubes so player must play cylinder
	state.UpdateFromAction(7, pieces.Stick, 0)
	state.UpdateFromAction(8, pieces.Stick, 0)
	state.UpdateFromAction(9, pieces.Stick, 0)
	state.UpdateFromAction(10, pieces.Stick, 0)
	state.UpdateFromAction(12, pieces.Stick, 0)
	state.UpdateFromAction(13, pieces.Stick, 0)
	state.UpdateFromAction(14, pieces.Stick, 0)
	state.UpdateFromAction(15, pieces.Stick, 0)
	// Prepare winning action
	state.UpdateFromAction(0, pieces.Stick, 0)
	state.UpdateFromAction(1, pieces.Stick, 0)
	state.UpdateFromAction(1, pieces.Cylinder, 0)
	state.UpdateFromAction(2, pieces.Cylinder, 0)
	state.UpdateFromAction(3, pieces.Cylinder, 0)
	// Just so it's player 0 turn again
	state.UpdateFromAction(6, pieces.Stick, 1)

	// When
	actions := state.GetNextActions()

	// Then
	// Since it returns all actions possible and not a single winning action, we expect a higher number
	assert.Equal(t, 83, len(actions))
}
