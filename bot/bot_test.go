package bot

import (
	"testing"

	"gitlab.com/natjo/interplay-bot/pieces"
)

func Not_TestRunExampleGame(t *testing.T) {
	// This should not throw an error
	// Given
	bot := NewBot(7, 7, 0)

	// Whe
	// Human turn
	bot.UpdateFromAction(0, pieces.Stick, 0)

	// Bot turn
	index, piece := bot.MakeDecision()
	bot.UpdateFromAction(index, piece, 1)

	// Human turn
	bot.UpdateFromAction(1, pieces.Stick, 0)

	// Bot turn
	index, piece = bot.MakeDecision()
	bot.UpdateFromAction(index, piece, 1)

	// Then
}
