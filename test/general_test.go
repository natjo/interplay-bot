package test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	mcts "gitlab.com/natjo/gomcts"
	"gitlab.com/natjo/interplay-bot/bot"
	"gitlab.com/natjo/interplay-bot/pieces"
)

func TestCorrectPlayerActions(t *testing.T) {
	// Given
	searchDepth := 10
	startingPlayer := 0
	state := bot.NewState(7, 7, int8(startingPlayer))
	tree := mcts.NewTree(state, searchDepth, startingPlayer, time.Millisecond*10)
	tree.SetThreads(4)

	// When
	tree.StartExplore()
	action0 := tree.GetBestAction()
	tree.UpdateFromAction(action0)
	tree.StartExplore()
	action1 := tree.GetBestAction()
	tree.UpdateFromAction(action1)
	tree.StartExplore()
	action2 := tree.GetBestAction()
	tree.UpdateFromAction(action2)
	tree.StartExplore()
	action3 := tree.GetBestAction()

	// Then
	assert.EqualValues(t, 0, action0.GetActor())
	assert.EqualValues(t, 1, action1.GetActor())
	assert.EqualValues(t, 0, action2.GetActor())
	assert.EqualValues(t, 1, action3.GetActor())
}

func TestDetectWinningMove(t *testing.T) {
	// Given
	searchDepth := 1
	startingPlayer := 0
	state := bot.NewState(7, 7, int8(startingPlayer))
	tree := mcts.NewTree(state, searchDepth, startingPlayer, time.Millisecond*50)
	tree.SetThreads(4)

	actions := []*bot.Action{
		// Bot has High, Low, Low, Low
		// Bot needs to place a High on the last position to win
		bot.NewAction(1, pieces.Stick, 0),
		bot.NewAction(10, pieces.Stick, 1),
		bot.NewAction(2, pieces.Cylinder, 0),
		bot.NewAction(11, pieces.Stick, 1),
		bot.NewAction(3, pieces.Cylinder, 0),
		bot.NewAction(12, pieces.Stick, 1),
		bot.NewAction(4, pieces.Cylinder, 0),
		bot.NewAction(13, pieces.Stick, 1),
	}

	for _, action := range actions {
		tree.UpdateFromAction(action)
	}

	// When
	tree.StartExplore()
	mctsAction := tree.GetBestAction()

	// Then
	action, _ := mctsAction.(*bot.Action)
	assert.EqualValues(t, 0, action.Player)
	assert.EqualValues(t, 5, action.Index)
	assert.Equal(t, pieces.Stick, action.Piece)
}

func TestDetectWinningMove2(t *testing.T) {
	// Given
	searchDepth := 1
	startingPlayer := 0
	state := bot.NewState(7, 7, int8(startingPlayer))
	tree := mcts.NewTree(state, searchDepth, startingPlayer, time.Millisecond*50)
	tree.SetThreads(4)

	actions := []*bot.Action{
		// Bot has High, High, Low, None, High
		// Bot needs to place a High on the second last position to win
		bot.NewAction(0, pieces.Stick, 0),
		bot.NewAction(1, pieces.Stick, 1),
		bot.NewAction(7, pieces.Stick, 0),
		bot.NewAction(2, pieces.Stick, 1),
		bot.NewAction(14, pieces.Cylinder, 0),
		bot.NewAction(3, pieces.Cylinder, 1),
		bot.NewAction(21, pieces.Stick, 0),
		bot.NewAction(4, pieces.Cylinder, 1),
	}

	for _, action := range actions {
		tree.UpdateFromAction(action)
	}

	// When
	tree.StartExplore()
	mctsAction := tree.GetBestAction()

	// Correct index 64, choosing 106

	// Then
	action, _ := mctsAction.(*bot.Action)
	assert.EqualValues(t, 0, action.Player)
	assert.EqualValues(t, 28, action.Index)
	assert.Equal(t, pieces.Stick, action.Piece)
}

func TestDetectWinningMove3(t *testing.T) {
	// Given
	searchDepth := 3
	startingPlayer := 0
	state := bot.NewState(7, 7, int8(startingPlayer))
	tree := mcts.NewTree(state, searchDepth, startingPlayer, time.Millisecond*50)
	tree.SetThreads(4)

	actions := []*bot.Action{
		// Bot has High, (Enemy High), High, (Enemy High), High
		bot.NewAction(0, pieces.Stick, 0),
		bot.NewAction(1, pieces.Stick, 1),
		bot.NewAction(2, pieces.Stick, 0),
		bot.NewAction(3, pieces.Stick, 1),
		bot.NewAction(4, pieces.Stick, 0),
		bot.NewAction(5, pieces.Stick, 1),
	}

	for _, action := range actions {
		tree.UpdateFromAction(action)
	}

	// When
	tree.StartExplore()
	mctsAction := tree.GetBestAction()

	// Then
	action, _ := mctsAction.(*bot.Action)
	assert.EqualValues(t, 0, action.Player)
	assert.Contains(t, []int16{1, 3}, action.Index)
	assert.Equal(t, pieces.Tube, action.Piece)
}

func TestDetectWinningMove4(t *testing.T) {
	// Given
	searchDepth := 3
	startingPlayer := 0
	state := bot.NewState(7, 7, int8(startingPlayer))
	tree := mcts.NewTree(state, searchDepth, startingPlayer, time.Millisecond*50)
	tree.SetThreads(4)

	actions := []*bot.Action{
		// Bot has High, (Enemy High), High, Low, High
		bot.NewAction(0, pieces.Stick, 0),
		bot.NewAction(1, pieces.Stick, 1),
		bot.NewAction(2, pieces.Stick, 0),
		bot.NewAction(3, pieces.Stick, 1),
		bot.NewAction(4, pieces.Stick, 0),
		bot.NewAction(5, pieces.Stick, 1),
		bot.NewAction(3, pieces.Tube, 0),
		bot.NewAction(6, pieces.Stick, 1),
	}

	for _, action := range actions {
		tree.UpdateFromAction(action)
	}

	// When
	tree.StartExplore()
	mctsAction := tree.GetBestAction()

	// Then
	action, _ := mctsAction.(*bot.Action)
	assert.EqualValues(t, 0, action.Player)
	assert.EqualValues(t, 1, action.Index)
	assert.Equal(t, pieces.Tube, action.Piece)
}

func TestDetectStopEnemyWin(t *testing.T) {
	// Given
	searchDepth := 2
	startingPlayer := 0
	state := bot.NewState(7, 7, int8(startingPlayer))
	tree := mcts.NewTree(state, searchDepth, startingPlayer, time.Millisecond*100)
	tree.SetThreads(3)

	actions := []*bot.Action{
		// Human has High, High, Low, High, None
		// Bot needs to place a High on the second last position to win
		bot.NewAction(0, pieces.Stick, 0),
		bot.NewAction(7, pieces.Stick, 1),
		bot.NewAction(1, pieces.Stick, 0),
		bot.NewAction(8, pieces.Stick, 1),
		bot.NewAction(2, pieces.Cylinder, 0),
		bot.NewAction(9, pieces.Tube, 1),
		bot.NewAction(3, pieces.Stick, 0),
	}

	for _, action := range actions {
		tree.UpdateFromAction(action)
	}

	// When
	tree.StartExplore()
	mctsAction := tree.GetBestAction()

	// Then
	action, _ := mctsAction.(*bot.Action)
	assert.EqualValues(t, 1, action.Player)
	assert.EqualValues(t, 4, action.Index)
	assert.Contains(t, []pieces.Piece{pieces.Stick, pieces.Cylinder}, action.Piece)
}
