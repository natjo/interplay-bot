package ui

import (
	"testing"

	"fyne.io/fyne/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/natjo/interplay-bot/pieces"
)

func TestTileToIcon(t *testing.T) {
	//Given
	type testParam struct {
		p    []pieces.Piece
		icon *fyne.StaticResource
	}
	parameters := []testParam{
		{p: []pieces.Piece{pieces.Stick, pieces.None}, icon: greenStick},
		{p: []pieces.Piece{pieces.None, pieces.Stick}, icon: yellowStick},
		{p: []pieces.Piece{pieces.Tube, pieces.None}, icon: greenTube},
		{p: []pieces.Piece{pieces.None, pieces.Tube}, icon: yellowTube},
		{p: []pieces.Piece{pieces.Cylinder, pieces.None}, icon: greenCylinder},
		{p: []pieces.Piece{pieces.None, pieces.Cylinder}, icon: yellowCylinder},
		{p: []pieces.Piece{pieces.Stick, pieces.Tube}, icon: yellowTubeGreenStick},
		{p: []pieces.Piece{pieces.Tube, pieces.Stick}, icon: greenTubeYellowStick},
		{p: []pieces.Piece{pieces.None, pieces.None}, icon: nil},
	}

	for _, params := range parameters {
		// When
		icon, err := getCorrectIcon(params.p)

		// Then
		assert.Equal(t, params.icon, icon)
		assert.Nil(t, err)
	}
}
func TestTileToIconError(t *testing.T) {
	//Given
	p := []pieces.Piece{pieces.Cylinder, pieces.Cylinder}

	// When
	icon, err := getCorrectIcon(p)

	// Then
	assert.Nil(t, icon)
	assert.NotNil(t, err)
}
