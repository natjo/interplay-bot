package ui

import (
	"errors"
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/interplay-bot/pieces"
)

// This struct manages the game field itself, which contains all the buttons which represent the tiles
type Tile struct {
	Button *widget.Button
}

func NewTile(index int, callback func(int)) *Tile {
	button := widget.NewButtonWithIcon("", nil, func() {
		callback(index)
	})
	return &Tile{Button: button}
}

func (tile *Tile) UpdatePieces(pieces []pieces.Piece) {
	// Sets the icon fitting to the pieces given as input. Always replace what is currently set
	icon, err := getCorrectIcon(pieces)
	if err != nil {
		logrus.Error(err)
		return
	}
	if icon == nil {
		// The UI library failes if we set an icon with the nil value. We must set an explicit nil value here
		tile.Button.SetIcon(nil)
	} else {
		tile.Button.SetIcon(icon)
	}
}

func (tile *Tile) MarkAsWinner() {
	tile.Button.Importance = widget.HighImportance
}

func getCorrectIcon(p []pieces.Piece) (*fyne.StaticResource, error) {
	// Player 0 is always green, player 1 is yellow
	if p[0] == pieces.None {
		if p[1] == pieces.Stick {
			return yellowStick, nil
		} else if p[1] == pieces.Tube {
			return yellowTube, nil
		} else if p[1] == pieces.Cylinder {
			return yellowCylinder, nil
		} else if p[1] == pieces.None {
			return nil, nil
		}
	} else if p[1] == pieces.None {
		if p[0] == pieces.Stick {
			return greenStick, nil
		} else if p[0] == pieces.Tube {
			return greenTube, nil
		} else if p[0] == pieces.Cylinder {
			return greenCylinder, nil
		}
	} else if p[0] == pieces.Stick && p[1] == pieces.Tube {
		return yellowTubeGreenStick, nil
	} else if p[0] == pieces.Tube && p[1] == pieces.Stick {
		return greenTubeYellowStick, nil
	}
	return nil, errors.New(fmt.Sprintf("Can't find an icon for %v & %v", pieces.PieceName(p[0]), pieces.PieceName(p[1])))
}
