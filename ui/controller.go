package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/interplay-bot/pieces"
)

// This is the main struct connecting the different ui game elements with each other
type Controller struct {
	pieceSelection       []*PieceSelection
	field                *Field
	currentPieceSelected []pieces.Piece
	sizeX                int
	sizeY                int
	callbackOnClick      func(int, []pieces.Piece)
}

func NewController(sizeX, sizeY int) *Controller {
	controller := &Controller{
		sizeX: sizeX,
		sizeY: sizeY,
		// We always start with Stick selection to keep things simple
		currentPieceSelected: []pieces.Piece{pieces.Stick, pieces.Stick},
	}

	controller.pieceSelection = []*PieceSelection{
		NewPieceSelection(0, controller.onPieceSelected),
		NewPieceSelection(1, controller.onPieceSelected),
	}
	controller.field = NewField(controller.sizeX, controller.sizeY, controller.onTileClick)

	return controller
}

// Field related
func (controller *Controller) SetCallbackOnClick(callback func(int, []pieces.Piece)) {
	controller.callbackOnClick = callback
}

func (controller *Controller) SetPiece(index int, pieces []pieces.Piece) {
	controller.field.SetPiece(index, pieces)
}

func (controller *Controller) UpdatePieceSelection(index int, piecesAmount map[pieces.Piece]int) {
	controller.pieceSelection[index].SetPiecesAmount(piecesAmount)
}

func (controller *Controller) MarkWinningTiles(tiles []int) {
	controller.field.MarkWinningTiles(tiles)
}

func (controller *Controller) GetContainer() *fyne.Container {
	// Returns the main view of the game
	return container.NewBorder(
		controller.GetPieceSelectionContainer(0),
		controller.GetPieceSelectionContainer(1),
		nil,
		nil,
		controller.GetFieldContainer(),
	)
}

func (controller *Controller) onTileClick(index int) {
	controller.callbackOnClick(index, controller.currentPieceSelected)
}

// Piece selection related
func (controller *Controller) GetPieceSelectionContainer(index int) *fyne.Container {
	// Returns the selection view of the game
	return controller.pieceSelection[index].Container
}

func (controller *Controller) GetFieldContainer() *fyne.Container {
	return controller.field.Container
}

func (controller *Controller) onPieceSelected(index int, piece pieces.Piece) {
	logrus.Debugf("Detected selection of %v for player %v", pieces.PieceName(piece), index)
	controller.currentPieceSelected[index] = piece
}
