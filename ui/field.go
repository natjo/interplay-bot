package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"gitlab.com/natjo/interplay-bot/pieces"
)

type Field struct {
	Container *fyne.Container
	tiles     []*Tile
	sizeX     int
	sizeY     int
	// There is player 0 and player 1, where player 0 starts by default
	currentPlayer int
	// Function called on click to receive which piece to set on a certain tile
	onTileClick func(int)
}

func NewField(sizeX, sizeY int, onTileClick func(int)) *Field {
	container := container.New(layout.NewGridLayout(sizeX))
	f := &Field{
		sizeX:         sizeX,
		sizeY:         sizeY,
		Container:     container,
		currentPlayer: 0,
		onTileClick:   onTileClick,
	}
	f.generateInitialTiles()
	return f
}

func (field *Field) generateInitialTiles() {
	for x := 0; x < field.sizeY; x++ {
		for y := 0; y < field.sizeX; y++ {
			index := field.getTileIndex(x, y)
			tile := NewTile(index, field.onTileClick)
			field.tiles = append(field.tiles, tile)
			field.Container.Add(tile.Button)
		}
	}
}

func (field *Field) SetPiece(index int, pieces []pieces.Piece) {
	field.tiles[index].UpdatePieces(pieces)
}

func (field *Field) MarkWinningTiles(tiles []int) {
	for _, i := range tiles {
		field.tiles[i].MarkAsWinner()
	}
}

func (field *Field) getTileIndex(x, y int) int {
	return x*field.sizeX + y
}
