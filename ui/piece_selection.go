package ui

import (
	"errors"
	"fmt"
	"strings"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/interplay-bot/pieces"
)

// This struct manages the selection of parts and the display of parts left for the player
type PieceSelection struct {
	selection *widget.RadioGroup
	Container *fyne.Container
}

func NewPieceSelection(index int, callback func(index int, x pieces.Piece)) *PieceSelection {
	pieceSelection := widget.NewRadioGroup([]string{"Stick[10]", "Tube[4]", "Cylinder[4]"}, func(value string) {
		logrus.Debugf("Selection of player %v set to %v", index, value)
		piece, err := selectionToPiece(value)
		if err != nil {
			logrus.Fatal(err)
		}
		callback(index, piece)
	})
	pieceSelection.Horizontal = true

	// Force a selected option and start with Stick
	pieceSelection.Required = true
	pieceSelection.Selected = "Stick[10]"

	playerLabel := widget.NewLabel(fmt.Sprintf("Player %v:", index))
	radioContainer := container.New(layout.NewCenterLayout(), pieceSelection)
	container := container.New(layout.NewHBoxLayout(), playerLabel, radioContainer)
	return &PieceSelection{selection: pieceSelection, Container: container}
}

func (pieceSelection *PieceSelection) GetCurrentSelection() pieces.Piece {
	piece, err := selectionToPiece(pieceSelection.selection.Selected)
	if err != nil {
		logrus.Fatal(err)
	}
	return piece
}

func (pieceSelection *PieceSelection) SetPiecesAmount(piecesAmount map[pieces.Piece]int) {
	options := pieceSelection.getOptionsStrings(piecesAmount)
	pieceSelection.selection.Options = options
	pieceSelection.refresh(piecesAmount)
}

func (pieceSelection *PieceSelection) refresh(piecesAmount map[pieces.Piece]int) {
	// Redraws the view, keeps the current selection
	currentSelection, _ := selectionToPiece(pieceSelection.selection.Selected)
	pieceSelection.selection.Selected = getLabel(currentSelection, piecesAmount[currentSelection])
	pieceSelection.selection.Refresh()
}

func (pieceSelection *PieceSelection) getOptionsStrings(piecesAmount map[pieces.Piece]int) []string {
	return []string{
		getLabel(pieces.Stick, piecesAmount[pieces.Stick]),
		getLabel(pieces.Tube, piecesAmount[pieces.Tube]),
		getLabel(pieces.Cylinder, piecesAmount[pieces.Cylinder]),
	}
}

func getLabel(piece pieces.Piece, amount int) string {
	return fmt.Sprintf("%v[%v]", pieces.PieceName(piece), amount)
}

func selectionToPiece(value string) (pieces.Piece, error) {
	if strings.HasPrefix(value, "Stick") {
		return pieces.Stick, nil
	} else if strings.HasPrefix(value, "Tube") {
		return pieces.Tube, nil
	} else if strings.HasPrefix(value, "Cylinder") {
		return pieces.Cylinder, nil
	}
	return pieces.Stick, errors.New(fmt.Sprintf("On radio selection, unkown piece selection %v", value))
}
