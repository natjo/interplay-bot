#!/bin/sh

DIR=$(dirname "$0")
FILE=ui/bundled_icons.go
MEDIA_DIR=$DIR/media/tiles

cd $DIR
rm $FILE

fyne bundle -package ui -name greenStick $MEDIA_DIR/green-stick.svg >> $FILE
fyne bundle -package ui -name yellowStick -append $MEDIA_DIR/yellow-stick.svg >> $FILE
fyne bundle -package ui -name greenTube -append $MEDIA_DIR/green-tube.svg >> $FILE
fyne bundle -package ui -name yellowTube -append $MEDIA_DIR/yellow-tube.svg >> $FILE
fyne bundle -package ui -name greenCylinder -append $MEDIA_DIR/green-cylinder.svg >> $FILE
fyne bundle -package ui -name yellowCylinder -append $MEDIA_DIR/yellow-cylinder.svg >> $FILE
fyne bundle -package ui -name yellowTubeGreenStick -append $MEDIA_DIR/yellow-tube-green-stick.svg >> $FILE
fyne bundle -package ui -name greenTubeYellowStick -append $MEDIA_DIR/green-tube-yellow-stick.svg >> $FILE